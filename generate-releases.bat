title generate-releases

mkdir mud-client-v1

go build -o "ipi-mud.exe"
set GOOS=linux
set GOARCH=amd64
go build -o "ipi-mud"

move /y ipi-mud mud-client-v1
move /y ipi-mud.exe mud-client-v1

if EXIST "mud-client-v1.zip" del /f /q "mud-client-v1.zip"

powershell.exe Compress-Archive -Path mud-client-v1 -DestinationPath ..\mud-client\mud-client-v1

rmdir /S /Q mud-client-v1