package main

import (
	"fmt"
	"net/http"
	"os"
)

func ConnectToServer() {
	//On recupère le nom du serveur et on indique le serveur au client
	var urlServer string = os.Getenv("IPIMUDSERVERURL")
	fmt.Println("Votre destination : " + urlServer)

	// on essaye de ce connecter si ça ne fonctionne pas message d'erreur sinon on affiche la reponse du serv A SUIVRE
	resp, err := http.Get("" + urlServer + ":3000/start")
	if err != nil {
		fmt.Println("Cannot connect to the server")
	} else {
		fmt.Println(resp)
	}

}

func main() {
	// Variables
	var playerName string
	var begin int
	var choix int

	// appelle la fonction de connexion au serveur (non fonctionnel sur la V0)
	ConnectToServer()
	fmt.Println("Vous etes le seul joueur présent dans la salle")

	// Choix du nom (les noms a espace ne sont pas gérer)
	fmt.Print("Bienvenue jeune aventurier, dites-moi, quel est votre nom ? : ")
	fmt.Scanln(&playerName)

	// Repétition du nom du joueur
	fmt.Printf("Mais oui !!! Comment ais-je pus t'oublier %s ! Cela fait fort longtemps que l'on ne s'est pas vu\n\n", playerName)

	// Choix des US imposés
	fmt.Println("Que voulez-vous faire ?")
	fmt.Println("\n1- Regarder autour de soit \n2- Vous vous grattez le nez \n3-Vous partez")
	fmt.Scanln(&choix)

	if choix == 1 { // Description du lieu
		fmt.Println("Vous êtes dans une belle ville, des gens discutent tout autour de vous, chantent et dansent")
		fmt.Println("Lse batiments sont colorés et cette ville est apaisante")
	}else if choix == 2{	// Grattage de nez
		fmt.Println("Et bien... vous vous grattez le nez, il est vrai que vous alliez éternuer")
	}else{	// Partir du jeu
		fmt.Println("Vous décidez de partir, au revoir")
		os.Exit(3)
	}

	// Demande au joueur si il veux jouer
	fmt.Println("\nBon c'est pas tout mais, es-tu pret pour une nouvelle aventure ?")
	fmt.Println("\n1- Oui \n2- Non \n3- Sortie du jeu")
	fmt.Scanln(&begin)

	// Si il veux jouer alors go, sinon fin du programme
	if begin == 1 {
		// variables
		var choixCombat int
		var vieJoueur int = 40
		var vieMob int = 50

		// Intro
		fmt.Println("\nAlors allons-y !!!!")
		fmt.Println("\nNous voici à l'entrée du chateau de fer")
		fmt.Printf("\n%s je vais devoir te laisser ici, bonne chance à toi !",playerName)

		// Description du lieu
		fmt.Println("Vous constatez que ce chateau mérite son nom, il est entiérement fait de fer, même la décoration, c'est assez sordide")

		// Choix de salle
		fmt.Println("Il y a 2 salles autour de vous, souhaitez vous aller dans la salle de droite ou de gauche ?")
		fmt.Println("\n1- Droite \n2- Gauche")
		fmt.Scanln(&choix)

		if choix == 1{
			// Début première salle
			fmt.Println("Attention il y a un monstre dans cette salle, vous prenez une épée de fer accrocher au mur")
			fmt.Println("Vous n'avez pas le choix que de l'attaquer")
			fmt.Println("Vous tuez le monstre mais pas sans une blessure")
			vieJoueur -= 5 // enleve 5pv
			fmt.Printf("Vous perdez 5 pv, il vous reste %d pv",vieJoueur)

			fmt.Println("\nVous ne pouvez aller que tout droit")
			// Fin de la première salle

			//Debut Boss
			fmt.Println("\nVous entrez dans la salle")
			fmt.Println("Un géant de fer se dresse devant vous, que faites vous ?")
			fmt.Println("1- Touchez le géant \n2- Examiner la salle")
			fmt.Scanln(&choix)

			switch choix {
			case 1 :
				fmt.Println("Le boss se reveille préparez vous à vous battre !")
				break
			case 2:
				fmt.Println("Vous trouvez une épée et un bouclier, c'est cool ça !")
				fmt.Println("Vous portez déjà une arme, vous prenez donc le bouclier")
				break
			default: fmt.Println("Vous ne savez pas suivre les consignes ? \nLe karma t'a foudroyé, tu es mort... \n Fin du jeu")
			}

			if choix == 1 { // Combat sans bouclier ni potion
				for{
					fmt.Printf("\nVous avez actuellement %d pv",vieJoueur)
					fmt.Printf("\nLe boss a actuellement %d pv",vieMob)

					fmt.Println("\nVous attaquez avec votre épée")

					fmt.Println("Vous infligez 5 points de degats et perdez 5pv")
					vieJoueur -= 5
					vieMob -= 5

					// Condition de fin de combat
					if vieJoueur <= 0 || vieMob <=0{
						break
					}
				}
			}else if choix == 2{ // Combat sans potion
				for{
					fmt.Printf("\nVous avez actuellement %d pv",vieJoueur)
					fmt.Printf("\nLe boss a actuellement %d pv",vieMob)

					fmt.Println("\nVous attaquez avec vos armes")

					fmt.Println("Vous infligez 5 points de degats et perdez 2pv")
					vieJoueur -= 2
					vieMob -= 5

					// Condition de fin de combat
					if vieJoueur <= 0 || vieMob <=0{
						break
					}
				}
			}
			//Fin Boss
		}else{
			// Début première salle
			fmt.Println("Vous trouvez une potion de soin")
			fmt.Println("Cette salle est vide, un vide qui fait peur, sortez vite de là, en face il y a une porte !")
			// Fin de la première salle

			//Debut Boss
			fmt.Println("\nVous entrez dans la salle")
			fmt.Println("Un géant de fer se dresse devant vous, que faites vous ?")
			fmt.Println("1- Touchez le géant \n2- Examiner la salle")
			fmt.Scanln(&choix)
			switch choix {
			case 1 :
				fmt.Println("Le boss se reveille préparez vous à vous battre !")
				break
			case 2:
				fmt.Println("Vous trouvez une épée et un bouclier, c'est cool ça !")
				fmt.Println("Vous vous équipez de tout ça")
				break
			default: fmt.Println("Vous ne savez pas suivre les consignes ? \nLe karma t'a foudroyé, tu es mort... \n Fin du jeu")
			}

			if choix == 1 {	// Combat sans armes ni bouclier
				for{
					fmt.Printf("\nVous avez actuellement %d pv",vieJoueur)
					fmt.Printf("\nLe boss a actuellement %d pv",vieMob)

					fmt.Println("\nQue faites vous ? :")
					fmt.Println("\n1- L'attaquer à main nue \n2- Utiliser votre potion de soin (regen 30pv)")
					fmt.Scanln(&choixCombat)

					if choixCombat == 1 {
						fmt.Println("Vous infligez 2 points de degats et perdez 5pv")
						vieJoueur -= 5
						vieMob -= 2
					}else{
						fmt.Println("Vous recupérer 30pv")
						if vieJoueur < 10 {
							vieJoueur += 30
						}else{
							vieJoueur = 40
						}
					}

					// Condition de fin de combat
					if vieJoueur <= 0 || vieMob <=0{
						break
					}
				}
			}else if choix == 2{ // Combat full équipé
				for{
					fmt.Printf("\nVous avez actuellement %d pv",vieJoueur)
					fmt.Printf("\nLe boss a actuellement %d pv",vieMob)

					fmt.Println("\nQue faites vous ? :")
					fmt.Println("\n1- L'attaquer avec vos armes \n2- Utiliser votre potion de soin (regen 30pv)")
					fmt.Scanln(&choixCombat)

					if choixCombat == 1 {
						fmt.Println("Vous infligez 5 points de degats et perdez 2pv")
						vieJoueur -= 2
						vieMob -= 5
					}else{
						fmt.Println("Vous recupérer 30pv")
						if vieJoueur < 10 {
							vieJoueur += 30
						}else{
							vieJoueur = 40
						}
					}

					// Condition de fin de combat
					if vieJoueur <= 0 || vieMob <=0{
						break
					}
				}
			}
			//Fin Boss
		}

		if(vieJoueur > 0){
			println("\n\nBien joué aventurié vous avez gagné, le jeu s'améliorera au fil du temps, attendez d'autres patch")
		}else{
			println("\n\nVous etes mort, dommage, essayez de nouveau")
		}
	} else if begin == 2 { // Fin du jeu prématurée
		fmt.Println("Bah pars alors, je ne veux plus te voir ici !")
		fmt.Println("Le karma t'a foudroyé, tu es mort... \n Fin du jeu")
	} else { // Si autre chose que 1 ou 2 est tapé fin du programme (il faudra empécher ça)
		fmt.Println("Sortie du jeu")
	}
}
